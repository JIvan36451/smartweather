package com.project.smartweather.ui.main.fragment

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.project.smartweather.databinding.MainFragmentBinding
import com.project.smartweather.ui.main.actions.WeatherActions
import com.project.smartweather.ui.main.model.WeatherDetail
import com.project.smartweather.ui.main.viewmodels.MainViewModel
import com.project.smartweather.ui.views.WeatherDayItemView
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder

class MainFragment : Fragment(),
    WeatherDayItemView.WeatherDayItemViewListener{

    private val groupAdapter = GroupAdapter<GroupieViewHolder>()

    private var listener: MainFragmentListener? = null

    private val binding: MainFragmentBinding by lazy {
        MainFragmentBinding.inflate(layoutInflater)
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View{
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? MainFragmentListener
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
        setUpRecyclerView()
        viewModel.getWeatherByCountry("2643743")
    }

    private fun setUpRecyclerView() {
        groupAdapter.clear()
        binding.recyclerContentData.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            setHasFixedSize(true)
            adapter = groupAdapter
        }
    }

    private fun bindViewModel() {
        viewModel.getWeatherActions().observe(viewLifecycleOwner, Observer(this::handleActions))
    }

    private fun handleActions(action: WeatherActions) {
        when (action) {
            is WeatherActions.ErrorWeatherData -> showErrorService(action.reason)
            is WeatherActions.SuccessWeatherData -> showRowWeatherInfo(action.list)
        }
    }

    private fun showErrorService(msg: String) {
        Toast.makeText(requireContext(), msg, Toast.LENGTH_LONG).show()
    }

    private fun showRowWeatherInfo(list: List<WeatherDetail>) {
        list.forEach {
            groupAdapter.add(
                WeatherDayItemView(
                    model = it,
                    listener = this
                )
            )
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onDetailDayWeather(model: WeatherDetail) {
        listener?.onDetailWeather(model)
    }

    interface MainFragmentListener {
        fun onDetailWeather(model: WeatherDetail)
    }
}