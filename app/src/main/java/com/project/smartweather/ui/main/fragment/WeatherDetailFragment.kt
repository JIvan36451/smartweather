package com.project.smartweather.ui.main.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.project.smartweather.databinding.WeatherDetailFragmentBinding

class WeatherDetailFragment : Fragment() {

    private val binding: WeatherDetailFragmentBinding by lazy {
        WeatherDetailFragmentBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root
}