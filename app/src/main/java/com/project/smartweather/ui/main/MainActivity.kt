package com.project.smartweather.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.project.smartweather.R
import com.project.smartweather.databinding.MainActivityBinding
import com.project.smartweather.ui.main.fragment.MainFragment
import com.project.smartweather.ui.main.fragment.MainFragmentDirections
import com.project.smartweather.ui.main.model.WeatherDetail

class MainActivity : AppCompatActivity(),
    MainFragment.MainFragmentListener{

    private val binding by lazy {
        MainActivityBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
    }

    private fun currentNavController(): NavController = findNavController(R.id.navHostFragment)

    override fun onSupportNavigateUp(): Boolean {
        return when (currentNavController().currentDestination?.id) {
            R.id.fragment_main_example -> {
                finish()
                false
            }
            else -> currentNavController().navigateUp()
        }
    }

    override fun onDetailWeather(model: WeatherDetail) {
        val direction = MainFragmentDirections.actionToWeatherDetail(model)
        currentNavController().navigate(direction)
    }
}