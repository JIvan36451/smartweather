package com.project.smartweather.ui.main.repository

import com.project.smartweather.ui.main.model.WeatherServiceResponse
import com.project.smartweather.ui.server.RetrofitClient
import com.project.smartweather.utils.applySchedulers
import io.reactivex.Single

class WeatherRepository {

    fun getWeather(country: String): Single<WeatherServiceResponse> {
        return RetrofitClient.instanceWeatherService.getWeatherByCountry(
            country = country,
            appid = KEY
        ).applySchedulers()
    }

    private companion object {
        const val KEY = "f2f9a557eed92d405c3f730b69329c68"
    }
}