package com.project.smartweather.ui.main.service

import com.project.smartweather.ui.main.model.WeatherServiceResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApiService {

    @GET("climate")
    fun getWeatherByCountry(
        @Query(COUNTRY) country: String,
        @Query(APPID) appid: String
    ): Single<WeatherServiceResponse>

    private companion object {
        const val COUNTRY = "id"
        const val APPID = "appid"
    }
}