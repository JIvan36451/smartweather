package com.project.smartweather.ui.main.model

import com.google.gson.annotations.SerializedName


data class Coord (

	@SerializedName("lon") val lon : Double,
	@SerializedName("lat") val lat : Double
)