package com.project.smartweather.ui.main.actions

import com.project.smartweather.ui.main.model.WeatherDetail

sealed class WeatherActions {
    data class SuccessWeatherData(val list: List<WeatherDetail>): WeatherActions()
    data class ErrorWeatherData(val reason: String): WeatherActions()
}