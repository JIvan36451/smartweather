package com.project.smartweather.ui.main.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.project.smartweather.ui.main.actions.WeatherActions
import com.project.smartweather.ui.main.repository.WeatherRepository
import com.project.smartweather.utils.SingleLiveEvent
import io.reactivex.disposables.CompositeDisposable

class MainViewModel : ViewModel() {
    private val disposable = CompositeDisposable()
    private val repository: WeatherRepository = WeatherRepository()

    private val action = SingleLiveEvent<WeatherActions>()
    fun getWeatherActions(): LiveData<WeatherActions> = action

    fun getWeatherByCountry(country: String) {
        disposable.add(
            repository.getWeather(country)
                .doOnSubscribe {
                    // Show Progress
                }
                .doFinally {
                    // hide Progress
                }
                .subscribe(
                    {
                        action.value = WeatherActions.SuccessWeatherData(it.weatherDetail)
                    },
                    {
                        action.value = WeatherActions.ErrorWeatherData(it.message.orEmpty())
                    })
        )
    }
}