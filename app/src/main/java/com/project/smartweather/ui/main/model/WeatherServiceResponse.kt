package com.project.smartweather.ui.main.model

import com.google.gson.annotations.SerializedName

data class WeatherServiceResponse (

	@SerializedName("city") val city : City,
	@SerializedName("code") val code : Int,
	@SerializedName("message") val message : Double,
	@SerializedName("cnt") val cnt : Int,
	@SerializedName("list") val weatherDetail : List<WeatherDetail>
)