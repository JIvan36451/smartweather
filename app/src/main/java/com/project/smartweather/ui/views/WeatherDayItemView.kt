package com.project.smartweather.ui.views

import android.annotation.SuppressLint
import android.view.View
import com.project.smartweather.R
import com.project.smartweather.databinding.WeatherDayItemViewBinding
import com.project.smartweather.ui.main.model.WeatherDetail
import com.xwray.groupie.viewbinding.BindableItem

class WeatherDayItemView(
    private val model: WeatherDetail,
    private val listener: WeatherDayItemViewListener
): BindableItem<WeatherDayItemViewBinding>() {

    @SuppressLint("SetTextI18n")
    override fun bind(viewBinding: WeatherDayItemViewBinding, position: Int) {
        viewBinding.apply {
            textViewDialogMessage.text ="Dia $position - Clima ${model.weather.first().description}"
            root.setOnClickListener {
                listener.onDetailDayWeather(model)
            }
        }
    }

    override fun getLayout() = R.layout.weather_day_item_view

    override fun initializeViewBinding(view: View) = WeatherDayItemViewBinding.bind(view)

    interface WeatherDayItemViewListener {
        fun onDetailDayWeather(model: WeatherDetail)
    }
}